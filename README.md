# [![Sia Logo](https://sia.tech/static/assets/svg/sia-green-logo.svg)](http://sia.tech)

The Sia project has moved to [GitHub](https://github.com/SiaFoundation).
